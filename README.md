# Arduino Project | Simon Game

## Matériel

1x arduino uno
1x speaker
1x led
1x move sensor (DFROBOT PAJ7620U2)

## Configuration

Brancher chaque composant au bon endroit sur la arduino en suivant le schéma présent :

![Simon Schema Arduino](./arduino_simon_schema.png)

Ouvrir le fichier 'simon_arduino' et 'pitches.h' sous l'IDE arduino puis téléverser le code vers la arduino.

Ouvrir le fichier 'simon_interface' depuis Processing, vérifier que le port de la arduino configuré correspond bien à celui de votre installation et lancer le programme.
