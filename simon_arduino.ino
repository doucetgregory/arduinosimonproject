#include <DFRobot_PAJ7620U2.h>
#include "pitches.h"

DFRobot_PAJ7620U2 moveSensor;

// Mélodies
int melodySong[] = {
  NOTE_E5, 4,  NOTE_B4,8,  NOTE_C5,8,  NOTE_D5,4,  NOTE_C5,8,  NOTE_B4,8,
  NOTE_A4, 4,  NOTE_A4,8,  NOTE_C5,8,  NOTE_E5,4,  NOTE_D5,8,  NOTE_C5,8,
  NOTE_B4, -4
};
int melodyGood[] = {
  NOTE_E5, 4,  NOTE_B4,8,  NOTE_C5,8,  NOTE_D5,4
};
int melodyError[] = {
  NOTE_C4, NOTE_C4, NOTE_C4, NOTE_G3, 0, NOTE_B3, NOTE_A3, NOTE_G3, NOTE_C4
};
int durationSong[] = {
  4, 8, 8, 4, 4, 4, 4, 4,4, 8, 8, 4, 4, 4, 4, 4, 4, 8, 8, 4, 4, 4, 4, 4, 4, 8
};
int durationGood[] = {
  4, 8, 8, 4, 4, 4, 4, 4
};
int durationError[] = {
  4, 4, 4, 8, 4, 4, 4, 4, 6
};

// Gestures
DFRobot_PAJ7620U2::eGesture_t scenario1[]={DFRobot_PAJ7620U2::eGestureLeft,DFRobot_PAJ7620U2::eGestureLeft,DFRobot_PAJ7620U2::eGestureUp};
DFRobot_PAJ7620U2::eGesture_t scenario2[]={DFRobot_PAJ7620U2::eGestureRight,DFRobot_PAJ7620U2::eGestureRight,DFRobot_PAJ7620U2::eGestureUp, DFRobot_PAJ7620U2::eGestureLeft, DFRobot_PAJ7620U2::eGestureDown};

static uint8_t indexGesture = 0;     //The number of the correctly input gesture
bool finish = false;
#define TIMEOUT 25000 //Set Timeout period, unit(mm)

const int ledPin =  13;      // the number of the LED pin
char response;

void setup()
{
  Serial.begin(9600);
  delay(300);
  pinMode(ledPin, OUTPUT);
  while(moveSensor.begin() != 0){
    Serial.println("Connection échoué ! (PAJ7620U2)");
    delay(500);
  }
  moveSensor.setGestureHighRate(true);
  // songMain();
}

void songMain() {
  for (int thisNote = 0; thisNote < 26; thisNote++) {
    int noteDuration = 1000 / durationSong[thisNote];
    tone(8, melodySong[thisNote], noteDuration);
    int pauseBetweenNotes = noteDuration * 1.30;
    delay(pauseBetweenNotes);
    noTone(8);
  }
}

void songGood() {
  for (int thisNote = 0; thisNote < 8; thisNote++) {
    int noteDuration = 1000 / durationGood[thisNote];
    tone(8, melodyGood[thisNote], noteDuration);
    int pauseBetweenNotes = noteDuration * 1.30;
    delay(pauseBetweenNotes);
    noTone(8);
  }
}

void toneError() {
    tone(8, NOTE_C4, 250);
}

void toneGood() {
    tone(8, NOTE_E5, 250);
}

void loop()
{
  DFRobot_PAJ7620U2::eGesture_t gesture;
  uint8_t pdLen = sizeof(scenario1)/sizeof(scenario1[0]);
  unsigned long startTimeStamp = millis();
  do{
    gesture = moveSensor.getGesture();
    if(gesture == moveSensor.eGestureNone){
      continue;
    }
    toneGood();
    Serial.println(moveSensor.gestureDescription(gesture));
  }while(!finish);
  digitalWrite(ledPin, HIGH);
  songGood();
}
