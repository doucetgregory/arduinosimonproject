import processing.serial.*;    // Importing the serial library to communicate with the Arduino 
import java.util.HashMap;
import java.util.Map;
import java.util.List;
import java.util.ArrayList;
import java.lang.*;
import java.util.Random;
import java.lang.Math; 

Serial myPort;

Thread threadGameOverTemplate = null;
Thread threadGameOverActuel = null;
int delayGameOver = 10000;
List<Integer> listeAction = new ArrayList<Integer>();
List<Integer> listeActionRestant = new ArrayList<Integer>();
Map<Integer, String> actionMap = new HashMap<Integer, String>();
boolean isInit = false;
boolean isWaitingAnswer = false;
boolean startGame = true;
boolean displayUp = false;
boolean displayDown = false;
boolean displayRight = false;
boolean displayLeft = false;
boolean inMenu = true;
boolean menuAffiche = false;
boolean isGameOver = false;
int level = 1;
int delaytotal = 1000;

void setup() {
   size(600, 600);
   background(51);
   frameRate(60);
   
   println(Serial.list());
   
   myPort = new Serial (this, "COM5", 9600); // Set the com port and the baud rate according to the Arduino IDE
   
   actionMap.put(new Integer(0), "Right");
   actionMap.put(new Integer(1), "Left");
   actionMap.put(new Integer(2), "Up");
   actionMap.put(new Integer(3), "Down");
   
   threadGameOverTemplate = new Thread(new Runnable() {
        public void run() {
          try{
            System.out.println("Start cooldown");
            Thread.sleep(delayGameOver);
            System.out.println("Stop cooldown");
            gameOver();
          }catch(Exception e){
            
          }
        }
   });

   generateLevelActions(level);
}

void serialEvent (Serial myPort) {
  if (!isWaitingAnswer) {
    return;
  }
  System.out.println("Liste actions :");
  System.out.println(listeActionRestant);
  if(listeActionRestant == null || listeActionRestant.size()==0){
    return;
  }
  String response = myPort.readStringUntil('\n');  // Changing the background color according to received data
  if (response != null) {
    System.out.println(response);
    if (!response.contains("30240")) {
    if(response.contains(actionMap.get(listeActionRestant.get(0)).toString())) {
      listeActionRestant.remove(0);
      arduinoCommuncation(true);
      if(listeActionRestant.size() > 0){
        relancerChrono();
      }else{
        interruptChrono();
        victoire();
      }
    }else{
        arduinoCommuncation(false);
        interruptChrono();
        gameOver();
    }
    }
  }
}

void arduinoCommuncation (boolean isOK) {
  myPort.write(Boolean.toString(isOK));
}


void play(){
  listeActionRestant = new ArrayList<Integer>(listeAction);
  print(listeActionRestant);
  relancerChrono();
}

void relancerChrono(){
  interruptChrono();
  threadGameOverActuel = new Thread(threadGameOverTemplate);
  threadGameOverActuel.start();
}

void interruptChrono(){
  if(threadGameOverActuel != null){
    threadGameOverActuel.interrupt();
  }
}

void gameOver(){
  inMenu = true;
  isInit = true;
  interruptChrono();
  isWaitingAnswer = false;
  listeActionRestant = null;
  menuAffiche = false;
  isGameOver = true;
  new Thread(new Runnable() {
    public void run() {
      try{
        Thread.sleep(4000);
      } catch(Exception e) {}
      isGameOver = false;
    }
  }).start();
  System.out.println("Game over grosse merde");
  listeAction = new ArrayList<Integer>();
  level = 1;
  generateLevelActions(level);
}

void victoire(){
  generateLevelActions(0);
  isInit = false;
  level++;
  isWaitingAnswer = false;
}

void draw() {
  if (!inMenu) {
    clear();
    if (!this.isInit) {
      isInit = true;
      new Thread(new Runnable() {
        public void run() {
          displayLevel();
        }
      }).start();
    } else if(this.isWaitingAnswer) {
        if(this.startGame) {
          startGame = false;
          play();
        }
        waitAnswer();
    } else {
      textSize(45);
      text("Niveau" + level, 210, 150);
      stroke(255);
      fill(255);
      if (displayRight) {
        // flèche vers la droite (rouge)
        arrow(200, 300, 400, 300, 255, 0, 0);
      }
      if (displayLeft) {
        // flèche vers la gauche (jaune)
        arrow(400, 300, 200, 300, 255, 255, 0);
      }
      if (displayUp) {
        // flèche vers le haut (vert)
        arrow(300, 400, 300, 200, 0, 255, 0);
      }
      if (displayDown) {
        // flèche vers le bas (bleu)
        arrow(300, 200, 300, 400, 0, 0, 255);
      }
    }
  } else if (isGameOver) {
    clear();
    background(51);
    textSize(45);
    text("Game Over", 183, 150);
    textSize(32);
    text("Niveau " + level + " atteint", 190, 250);
    stroke(255);
    fill(255);
  } else if (!menuAffiche) {
    menuAffiche = true;
    clear();
    initMenu();
  }
}

void waitAnswer() {
  clear();
  textSize(45);
  text("Réponse ?", 190, 150);
  stroke(255);
  fill(255);
  if(listeActionRestant != null )
    text(listeAction.size() - listeActionRestant.size() + " / " + listeAction.size(), 240, 250);
}

void initMenu() {
  background(51);
  textSize(32);
  fill(255);
  text("LES DIEUX DE L'UML", 147, 150);
  stroke(255);
  fill(255);
  rect(200, 275, 200, 50);
  textSize(25);
  fill(51);
  text("Start", 272, 309);
}

void mouseClicked() {
  if (mouseX>200 && mouseX<400 &&
    mouseY>275 && mouseY<325) {
    inMenu = false;
    isInit = false;
    isWaitingAnswer = false;
    startGame = true;
    displayUp = false;
    displayDown = false;
    displayRight = false;
    displayLeft = false;
    menuAffiche = false;
  }
}

void displayLevel() {
  delaytotal = (int)(1500 - sqrt((float)Math.pow(level,2.5)*(300000/((level*level)+1))));
  if(delaytotal<200){
    delaytotal = 200;
  }
  for(Integer i : this.listeAction) {
    if (actionMap.get(i).equals("Right")) {
      System.out.print("right");
      displayRight = true;
      displayLeft = false;
      displayUp = false;
      displayDown = false;
      new Thread(new Runnable() {
        public void run() {
           try{
            Thread.sleep(delaytotal-100);
           }catch(Exception e){
              
           }
           displayRight = false;
        }
      }).start();
    }
    if (actionMap.get(i).equals("Left")) {
      System.out.print("left");
      displayRight = false;
      displayLeft = true;
      displayUp = false;
      displayDown = false;
      new Thread(new Runnable() {
        public void run() {
           try{
            Thread.sleep(delaytotal-100);
           }catch(Exception e){
              
           }
           displayLeft = false;
        }
      }).start();
    }
    if (actionMap.get(i).equals("Up")) {
      System.out.print("up");
      displayRight = false;
      displayLeft = false;
      displayUp = true;
      displayDown = false;
      new Thread(new Runnable() {
        public void run() {
           try{
            Thread.sleep(delaytotal-100);
           }catch(Exception e){
              
           }
           displayUp = false;
        }
      }).start();
    }
    if (actionMap.get(i).equals("Down")) {
      System.out.print("down");
      displayRight = false;
      displayLeft = false;
      displayUp = false;
      displayDown = true;
      new Thread(new Runnable() {
        public void run() {
           try{
            Thread.sleep(delaytotal-100);
           }catch(Exception e){
              
           }
           displayDown = false;
        }
      }).start();
      
    }
    redraw();
    try {
      Thread.sleep(delaytotal);
    } catch (Exception e) {
      System.out.println("Erreur");
    }
  }
  isWaitingAnswer = true;
  startGame = true;
}

void generateLevelActions(int level) {
  for(int i = 0; i <= level; i++) {
    Random rand = new Random(); //instance of random class
    int upperbound = 4;
    //generate random values from 0-3
    int int_random = rand.nextInt(upperbound);
    listeAction.add(new Integer(int_random));
  }
}

void arrow(int x1, int y1, int x2, int y2, int r, int g, int b) {
  stroke(r, g ,b);
  strokeWeight(15);
  line(x1, y1, x2, y2);
  pushMatrix();
  translate(x2, y2);
  float a = atan2(x1-x2, y2-y1);
  rotate(a);
  line(0, 0, -50, -50);
  line(0, 0, 50, -50);
  popMatrix();
  redraw();
}
